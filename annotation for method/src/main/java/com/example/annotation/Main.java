package com.example.annotation;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {

    public static void main(String[] args) {
        final Class<?> cls = Annotation.class;
        Method[] methods = cls.getDeclaredMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(Test.class)) {
                System.out.println("Аннотированный метод - " + method.getName() + ";");
                Test an = method.getAnnotation(Test.class);
                try {
                    method.invoke(new Annotation(),an.a(),an.b());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

