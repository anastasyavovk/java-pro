package com.example.annotation;

public class Annotation {
    public Annotation(){
    }
    @Test(a=2, b=5)
    public void test(int a, int b) {
        System.out.println("Параметр a = " + a + ";");
        System.out.println("Параметр b = " + b + ";");
    }
    @Override
    public String toString() {
        return "TestAnnotation{}";
    }
}
