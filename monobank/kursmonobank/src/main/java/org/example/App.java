package org.example;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
public class App {
    public static void main(String[] args) throws IOException{
        String urlJSON = getJSON("https://api.monobank.ua/bank/currency");
        System.out.println(getExhangeRate(urlJSON) + " UAH");
    }

    public static String getJSON(String s) throws IOException{
        String infoJson = "";
        URL url = new URL(s);
        URLConnection urlCon = (HttpURLConnection) url.openConnection();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlCon.getInputStream()))){
            infoJson = bufferedReader.readLine();
        } catch (IOException ex){
            ex.printStackTrace();
        }
        return infoJson;
    }

    public static double getExhangeRate(String infoJson){
        double rateCurrency = 0.0;
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Currency[] currency = gson.fromJson(infoJson, Currency[].class);
        for (Currency cur : currency) {
            if(cur.getCurrencyCodeA() == 840 && cur.getCurrencyCodeB() == 980){
                rateCurrency = cur.getRateBuy();
            }
        }
        return rateCurrency;
    }
}