import java.sql.*;
import java.util.Scanner;

public class Search {
    private final String urlConnectionSql;
    private final String userSql;
    private final String passwordSql;
    private Connection con;

    public Search(String urlConnectionSql, String userSql, String passwordSql){
        this.urlConnectionSql = urlConnectionSql;
        this.userSql = userSql;
        this.passwordSql = passwordSql;
    }

    public void begin() {
        try {
            try (Scanner sc = new Scanner(System.in)){
                con = DriverManager.getConnection(urlConnectionSql, userSql, passwordSql);
                initDB();
                getTable("");
                while (true) {
                    System.out.println(
                            """
                                    для добавления нового значения - выберите «1»
                                    для поиска с параметрами - выберите "2"
                                    ->"""
                    );
                    switch (sc.nextLine()) {
                        case "1" -> insert(sc);
                        case "2" -> getTable(createStatement(sc));
                        default -> getTable("");
                    }
                }
            } catch (SQLException e){
                e.printStackTrace();
            } finally {
                if (con != null) con.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void initDB() throws SQLException{
        try (Statement st = con.createStatement()){
            st.execute(
                    "CREATE TABLE IF NOT EXISTS Flats (" +
                            "ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY, " +
                            "REGION VARCHAR(20) NOT NULL, " +
                            "ADDRESS VARCHAR(50) NOT NULL, " +
                            "AREA DOUBLE NOT NULL, " +
                            "ROOMS INT NOT NULL, " +
                            "PRICE DOUBLE NOT NULL)"
            );
        }
    }

    public void insert(Scanner sc) throws SQLException{
        System.out.print("Укажите регион: ");
        String region = sc.hasNextLine() ? sc.nextLine() : "";
        System.out.print("Укажите адрес: ");
        String address = sc.hasNextLine() ? sc.nextLine() : "";
        System.out.print("Укажите квадратуру: ");
        double area = sc.hasNextDouble() ? sc.nextDouble() : 0;
        System.out.print("Укажите количество комнат: ");
        int rooms = sc.hasNextInt() ? sc.nextInt() : 0;
        System.out.print("Укажите стоимость: ");
        double price = sc.hasNextDouble() ? sc.nextDouble() : 0;

        try (PreparedStatement ps = con.prepareStatement(
                "INSERT INTO Flats (REGION, ADDRESS, AREA, ROOMS, PRICE) VALUES(?, ?, ?, ?, ?)")) {
            ps.setString(1, region);
            ps.setString(2, address);
            ps.setDouble(3, area);
            ps.setInt(4, rooms);
            ps.setDouble(5, price);
            ps.executeUpdate();
        }
    }

    public String createStatement(Scanner scanner) {
        double tmp;
        String statement =
                "SELECT * FROM Flats WHERE " +
                        "(AREA BETWEEN ";
        System.out.println("Укажите минимальную площадь квартиры: ");
        tmp = scanner.hasNextDouble() ? scanner.nextDouble() : 0;
        statement += "" + tmp + " AND ";
        System.out.println("Укажите максимальную площадь квартиры: ");
        tmp = scanner.hasNextDouble() ? scanner.nextDouble() : 500;
        statement +=
                "" + tmp + ")" +
                        " AND (ROOMS BETWEEN ";
        System.out.println("Укажите минимальное кол-во комнат: ");
        tmp = scanner.hasNextInt() ? scanner.nextInt() : 0;
        statement += "" + tmp + " AND ";
        System.out.println("Укажите максимальное кол-во комнат: ");
        tmp = scanner.hasNextInt() ? scanner.nextInt() : 500;
        statement +=
                "" + tmp + ")" +
                        " AND (PRICE BETWEEN ";
        System.out.println("Укажите минимальную стоимость: ");
        tmp = scanner.hasNextInt() ? scanner.nextInt() : 0;
        statement += "" + tmp + " AND ";
        System.out.println("Укажите максимальную стоимость: ");
        tmp = scanner.hasNextInt() ? scanner.nextInt() : 100_000;
        statement +=
                "" + tmp + ")";

        System.out.println(statement);
        return statement;
    }

    private void getTable(String statement) throws SQLException {
        try (
                PreparedStatement ps = con.prepareStatement(
                        statement.equals("") ? "SELECT * FROM Flats" : statement);
                ResultSet rs = ps.executeQuery()
        ) {
            ResultSetMetaData md = rs.getMetaData();

            for (int i = 1; i <= md.getColumnCount(); i++)
                System.out.print(md.getColumnName(i) + "\t\t");
            System.out.println();

            while (rs.next()) {
                for (int i = 1; i <= md.getColumnCount(); i++) {
                    System.out.print(rs.getString(i) + "\t\t");
                }
                System.out.println();
            }
        }
    }
}