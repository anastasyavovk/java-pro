import javax.persistence.*;
import java.util.*;

public class Menu{
    static EntityManagerFactory entityMF;
    static EntityManager entityM;

    public static void openEm(){
        entityMF = Persistence.createEntityManagerFactory("JPATest");
        entityM = entityMF.createEntityManager();
    }
    public static void closeEm(){
        entityM.close();
        entityMF.close();
    }

    public static void addChoice(){
        Scanner sc = new Scanner(System.in);
        openEm();
        System.out.println("Name: " );
        String name = sc.nextLine();
        System.out.println("Price: ");
        String priceStr = sc.nextLine();
        Double price = Double.parseDouble(priceStr);
        System.out.println("Weight in grams: ");
        String weightStr = sc.nextLine();
        Double weight = Double.parseDouble(weightStr);
        System.out.println("Discount 1 or 2: ");
        String discountStr = sc.nextLine();
        String discountString;
        if (discountStr.equals("1".toLowerCase())){
            discountString = "true";
        }else{
            discountString = "false";
        }
        boolean isDiscountHave = Boolean.parseBoolean(discountString);

        entityM.getTransaction().begin();
        try{
            Goods goods = new Goods(name,price,weight,isDiscountHave);
            entityM.persist(goods);
            entityM.getTransaction().commit();
            System.out.println("Goods id is: " + goods.getId());
        }catch(Exception e){
            entityM.getTransaction().rollback();
        }
    }
    public static void filterByPrice(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Minimum price, $: ");
        String minPriceStr = sc.nextLine();
        Double minPrice = Double.parseDouble(minPriceStr);
        System.out.println("Maximum price in $: ");
        String maxPriceStr = sc.nextLine();
        Double maxPrice = Double.parseDouble(maxPriceStr);

        openEm();
        TypedQuery query = entityM.createQuery("SELECT g FROM Goods g WHERE g.price > :minPrice AND g.price < :maxPrice", Goods.class);
        query.setParameter("minPrice",minPrice);
        query.setParameter("maxPrice",maxPrice);

        List<Goods> ListA = query.getResultList();

        for (Goods goods:ListA){
            System.out.println(goods);
        }
    }
    public static void priceWithDiscount(){
        openEm();
        TypedQuery query = entityM.createQuery("SELECT goods FROM Goods goods WHERE goods.isDiscountHave = true", Goods.class);
        List<Goods> ListA = query.getResultList();

        for (Goods goods : ListA){
            System.out.println(goods);
        }
    }
    public static void lessOneKg(){
        final int maxWeight = 1000;

        openEm();
        TypedQuery query = entityM.createQuery("SELECT goods FROM Goods goods", Goods.class);
        List<Goods> list = query.getResultList();
        Set<Goods> menuSet = new HashSet<>();
        Random rd = new Random();
        double weight;
        double totalWeight = 0;
        System.out.println("The menu of meal less then 1 kg: ");
        while (totalWeight <= maxWeight){
            Goods rdGoods = list.get(rd.nextInt(list.size()));
            weight = rdGoods.getWeight();
            if ((totalWeight + weight) < maxWeight){
                menuSet.add(rdGoods);
            }
            totalWeight += weight;

        }
        totalWeight = 0;
        for (Goods goods : menuSet){
            totalWeight += goods.getWeight();
        }
        for (Goods goods : list){
            if (!menuSet.contains(goods) && goods.getWeight() < (1000 - totalWeight)){
                menuSet.add(goods);
                totalWeight += goods.getWeight();
            }
        }
        for (Goods goods : menuSet){
            System.out.println(goods);
            System.out.println("Total weight - " + totalWeight + "grams");

        }
    }

}