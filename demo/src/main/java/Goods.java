import javax.persistence.*;

@Entity
@Table(name = "goods")
public class Goods {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "Name")
    private String name;
    @Column(name = "Price")
    private double price;
    @Column(name = "Weight")
    private double weight;
    @Column(name = "Discout")
    private boolean isDiscountHave;

    public Goods(){}

    public Goods(String name, double price, double weight, boolean isDiscountHave) {
        this.name = name;
        this.price = price;
        this.weight = weight;
        this.isDiscountHave = isDiscountHave;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public boolean isDiscountHave() {
        return isDiscountHave;
    }

    public void setDiscountHave(boolean discountHave) {
        isDiscountHave = discountHave;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", weight=" + weight +
                ", isDiscountHave=" + isDiscountHave +
                '}';
    }
}
