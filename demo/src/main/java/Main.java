import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        try{
            while(true){
                System.out.println("1: Add your choice: ");
                System.out.println("2: Filter menu by price: ");
                System.out.println("3: See all goods what have discount: ");
                System.out.println("4: Choose random menu less than 1 kg: ");
                System.out.print(">>>");

                String s = sc.nextLine();
                switch (s){
                    case "1":
                        Menu.addChoice();
                        break;
                    case "2":
                        Menu.filterByPrice();
                        break;
                    case "3":
                        Menu.priceWithDiscount();
                        break;
                    case "4":
                        Menu.lessOneKg();
                        break;
                    default:
                        return;
                }
            }
        }finally {
            sc.close();
            Menu.closeEm();
        }
    }
}