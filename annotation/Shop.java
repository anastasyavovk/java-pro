public class Shop {
    private String productType;
    private int quantityOfGoods;
    private int weight;
    private String color;

    public Shop(String productType, int quantityOfGoods, int weight, String color){
        super();
        this.productType = productType;
        this.quantityOfGoods = quantityOfGoods;
        this.weight = weight;
        this.color = color;
    }

    public Shop(){
        super();
    }

    public String getProductType() {
        return productType;
    }
    @ShopAnnotation
    public void setProductType(@GoodsAnnotation String productType) {
        this.productType = productType;
    }
    @ShopAnnotation
    public int getQuantityOfGoods() {
        return quantityOfGoods;
    }
    @ShopAnnotation
    public void setQuantityOfGoods(@GoodsAnnotation int quantityOfGoods) {
        this.quantityOfGoods = quantityOfGoods;
    }
    @ShopAnnotation
    public int getWeight() {
        return weight;
    }
    @ShopAnnotation
    public void setWeight(int weight) {
        this.weight = weight;
    }
    @ShopAnnotation
    public String getColor() {
        return color;
    }
    @ShopAnnotation
    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Shop{" +
                "productType='" + productType + '\'' +
                ", quantityOfGoods=" + quantityOfGoods +
                ", weight=" + weight +
                ", color='" + color + '\'' +
                '}';
    }
}
