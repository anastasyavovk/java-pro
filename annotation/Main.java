

public class Main {
    public static void main(String[] args) throws NoSuchMethodException {
        Goods firstClass = new Goods("oven", 800, 31, 30, "black");
        CountStatistic.countAnnotatedMethods(firstClass, ShopAnnotation.class);
        CountStatistic.countAnnotatedParameter(firstClass, GoodsAnnotation.class);
    }
}
