import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class CountStatistic {
    public static <T> int countAnnotatedMethods(T firstClass, Class<? extends Annotation> shopAnnotationClass) {
        int count = 0;
        Class<?> cls = firstClass.getClass();
        while (!cls.equals(Object.class)) {
            for (Method method : cls.getDeclaredMethods()) {
                if (method.isAnnotationPresent(shopAnnotationClass)) {
                    count++;
                }
            }
            cls = cls.getSuperclass();
        }
        System.out.println("Количество использований " + shopAnnotationClass + ": " + count + ".");
        return count;
    }
    public static <T> int countAnnotatedParameter(T firstClass, Class<? extends Annotation> goodsAnnotationClass) {
        int count = 0;
        Class<?> cls = firstClass.getClass();
        Method[] methods = cls.getMethods();
        for (Method method : methods) {
            Parameter[] parameters = method.getParameters();
            for (Parameter parameter : parameters) {
                Annotation[] annotations = parameter.getAnnotations();
                int numAnnotations = annotations.length;
                if(numAnnotations != 0){
                    count += numAnnotations;
                }
            }
        }
        System.out.println("Количество использований " + goodsAnnotationClass + ": " + count + ".");
        return count;
    }
}
