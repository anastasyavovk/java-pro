public class Goods extends Fridge {
    private int id;

    public Goods(String productType, int id, int quantityOfGoods, int weight, String color){
        super(productType, quantityOfGoods, weight, color);
        this.id = id;
    }

    public int getId() {
        return id;
    }
    @ShopAnnotation
    public void setId(int id) {
        this.id = id;
    }
    @ShopAnnotation
    public String getType(){
        return "Компрессионный";
    }
    @ShopAnnotation
    public String getManufacture(){
        return "LG";
    }

    @Override
    public String toString() {
        return "Goods{" +
                "id=" + id +
                ", priceSegment='" + priceSegment + '\'' +
                ", productType='" + getProductType() + '\'' +
                ", quantityOfGoods'" + getQuantityOfGoods() + '\'' +
                ", weight'" + getWeight() + '\'' +
                ", color'" + getColor() + '\'' +
                ", getType()'" + getType() + '\'' +
                ", getManufacture()'" + getManufacture()+ '\'' +
                '}';
    }
}
