public class Fridge extends Shop{
    String priceSegment;

    public Fridge (String productType, int quantityOfGoods, int weight, String color){
        super(productType, quantityOfGoods, weight, color);
    }

    public String getPriceSegment() {
        return priceSegment;
    }

    public void setPriceSegment(String priceSegment) {
        this.priceSegment = priceSegment;
    }

    @Override
    public String toString() {
        return "Shop [priceSegment=" + getPriceSegment() + " productType=" + getProductType() + ", quantityOfGoods=" + getQuantityOfGoods() + ", weight=" + getWeight() + ", color=" + getColor() + "]";
    }
}
