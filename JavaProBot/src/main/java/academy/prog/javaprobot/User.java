package academy.prog.javaprobot;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;


@Entity
public class User {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private Long chatId;
    @Column(nullable = false)
    private Long state;

    @Column(nullable = false)
    private Boolean admin;

    private String phone;//номер користувача
    private String pib;//ПІБ користувача
    private String city;//місто користувача
    private String numberPost;//номер відділення НП

    public User() {}

    public void incrementState(){
        state++;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getState() {
        return state;
    }

    public void setState(Long state) {
        this.state = state;
    }

    public String getPib() {
        return pib;
    }

    public void setPib(String pib) {
        this.pib = pib;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getNumberPost() {
        return numberPost;
    }

    public void setNumberPost(String numberPost) {
        this.numberPost = numberPost;
    }
}
