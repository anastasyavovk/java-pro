package academy.prog.javaprobot;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendAnimation;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.List;
import java.util.Optional;

@Component
public class MySuperBot extends TelegramLongPollingBot {

    private final static int USER_COUNT = 100;

    private final BotCredentials botCredentials;
    private final UserService userService;

    public MySuperBot(TelegramBotsApi telegramBotsApi,
                      BotCredentials botCredentials,
                      UserService userService) {
        super(botCredentials.getBotToken());
        this.botCredentials = botCredentials;
        this.userService = userService;

        try {
            telegramBotsApi.registerBot(this);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (!update.hasMessage() || !update.getMessage().hasText())
            return;

        String text = update.getMessage().getText();
        long chatId = update.getMessage().getChatId();

        Optional<User> userOpt = userService.findUserByChatId(chatId);
        User user;

        if(userOpt.isPresent()){
            user = userOpt.get();

            if (isAdminCommand(text) && user.getAdmin()) {
                if (text.startsWith("/broadcast") || text.startsWith("/broadcast ")) {
                    processAdminCommand(chatId, text);
                }

                // робимо setadmin
                if (text.startsWith("/setadmin") || text.startsWith("/setadmin ")){
                    setAdminCommand(text, chatId);
                    return;
                }
                return;
            }
            //видаляємо юзера
            if (isDeleteCommand(text)){
                deleteUser(chatId);
                return;
            }
//відповіді та питання користувачу:
            if(user.getState() == 1){
                boolean valid = Utils.isValidUkrainianPhoneNumber(text);
                if(valid) {
                    user.setPhone(text);
                    sendMessage(chatId, "Додали. Вкажіть Ваш ПІБ(прізвище, ім'я та по-батькові.");
                    user.incrementState();
                }else {
                    sendMessage(chatId, "Неправильний телефон, вкажіть ще раз.");
                }
            }else if(user.getState() == 2){
                user.setPib(text);
                sendMessage(chatId, "Додали. Вкажіть Ваше місто.");
                user.incrementState();
            }else if (user.getState() == 3){
                user.setCity(text);
                sendMessage(chatId, "Додали. Вкажіть номер відділленя Нової пошти.");
                user.incrementState();
            }else if (user.getState() == 4){
                boolean valid = Utils.isValidNumberPost(text);
                if(valid){
                    user.setNumberPost(text);
                    sendMessage(chatId, "Всі дані додано. Дякуємо за Ваше замовлення, очікуйте номер накладної!");
                    sendAnimationDone(chatId, "https://cdn.dribbble.com/users/1465772/screenshots/5726748/____dri.gif");
                    user.incrementState();
                }else
                    sendMessage(chatId, "Невірний номер відділення, вкажіть ще раз.");
            }
            userService.updateUser(user);

        }else {
            String[] parts = text.split(" ");
            String password = (parts.length == 2) ? parts[1] : "";

            sendImageFromUrl(chatId, "https://assets.vogue.com/photos/640b4892ec80747033002737/master/w_2560%2Cc_limit/00-story.jpg");
            sendMessage(chatId, "Привіт! Я чат бот для оформлення замовлення.");
            sendMessage(chatId, "Вкажіть Ваш мобільний номер (380XXYYYYYYY).");

            user = new User();
            user.setAdmin(isValidPassword(password));
            user.setChatId(chatId);
            user.setState(1L);

            userService.saveUser(user);
        }
    }

    private void setAdminCommand(String text, long chatId) {
        String[] parts = text.split(" ");
        if (parts.length == 2) {
            String userId = parts[1];
            setAdminById(userId, chatId);
        } else {
            sendMessage(chatId, "Невірна команда адміна. Наприклад: /setadminbyid 2 (де 2 - номер ID)");
        }
    }

    private void setAdminById(String userId, long chatId) {
        var adminOptional = userService.findUserById(Long.parseLong(userId));
        if (adminOptional.isPresent()) {
            User adminUser = adminOptional.get();
            adminUser.setAdmin(true);
            userService.updateUser(adminUser);
            sendMessage(chatId, "Користувач з ID " + userId + " отримав права адміністратора.");
        }
        else {
            sendMessage(chatId, "Користувача не знайдено.");
        }
    }

    private void deleteUser(long chatId) {
        userService.deleteUser(chatId);
        sendMessage(chatId,"Ваш акаунт видалено.");
        sendImageFromUrl(chatId, "https://images.unsplash.com/photo-1636193535246-a07cd0aa6fcb?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8Z29vZGJ5ZXxlbnwwfHwwfHw%3D&w=1000&q=80");
    }

    private boolean isDeleteCommand(String text) {
        return text.startsWith("/delete ") || text.startsWith("/delete");
    }

    private boolean isValidPassword(String password) {
        return "1".equals(password);
    }

    //розсилка адмін (пишемо команду /broadcast і далі добавляємо текст, який отримають всі юзери боту)
    private void processAdminCommand(long chatId, String text) {

        int idx = text.indexOf(' ');
        if(idx < 0){
            sendMessage(chatId, "Невірна команда адміна.");
        }
        String message = text.substring(idx + 1);

        long usersCount = userService.getUsersCount();
        long pagesCount = (usersCount/USER_COUNT) + ((usersCount % USER_COUNT > 0) ? 1 : 0);

        for (int i = 0; i<pagesCount; i++){
            List<User> users = userService.findAllUsers(
                    PageRequest.of(i, USER_COUNT)
            );
            users.forEach(user -> {
                sendMessage(user.getChatId(), message);
            });
        }
    }

    private boolean isAdminCommand(String text) {
        return text.startsWith("/broadcast ") || text.startsWith("/setadmin ") || text.startsWith("/deleteuser ");
    }

    private void sendMessage(long chatId, String message){
        var msg = new SendMessage();
        msg.setText(message);
        msg.setChatId(chatId);

        try {
            execute(msg);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
    public void sendImageFromUrl(Long chatId, String url) {
        SendPhoto sendPhotoRequest = new SendPhoto();
        sendPhotoRequest.setChatId(chatId);
        sendPhotoRequest.setPhoto(new InputFile(url));
        try {
            execute(sendPhotoRequest);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public void sendAnimationDone(Long chatId, String url){
        SendAnimation sendAnimationRequest = new SendAnimation();
        sendAnimationRequest.setChatId(chatId);
        sendAnimationRequest.setAnimation(new InputFile(url));
        try {
            execute(sendAnimationRequest);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }


    @Override
    public String getBotUsername() {
        return botCredentials.getBotName();
    }
}
