module com.example.annotation {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.annotation to javafx.fxml;
    exports com.example.annotation;
}