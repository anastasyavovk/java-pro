package com.example.annotation;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Save{
    public static void saveFile(Container container){
        if (!container.getClass().isAnnotationPresent(SaveIn.class)){
            System.out.println(container.getClass()+" не анотований клас.");
            return;
        }
        SaveIn st = container.getClass().getAnnotation(SaveIn.class);
        System.out.println("Где искать (параметр): " + st.path());
        String s = st.path();
        Method[] methods = container.getClass().getDeclaredMethods();
        for (Method method : methods)
            if (method.isAnnotationPresent(Saver.class)){
                System.out.println("Аннотированный метод " + method.getName() + "!");
                try {
                    method.invoke(container,s);
                } catch (IllegalAccessException e){
                    e.printStackTrace();
                } catch (InvocationTargetException e){
                    e.printStackTrace();
                }
            }
    }
}
