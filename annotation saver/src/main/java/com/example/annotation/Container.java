package com.example.annotation;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

@SaveIn(path = "C:\\Users\\Sergey\\IdeaProjects\\annotation saver\\text.txt")
public class Container{
    private String text;

    public Container(){
    }
    public Container(String text){
        this.text = text;
    }

    @Saver
    public void save(String f) {
        try (PrintWriter pw = new PrintWriter(f)){
            pw.println(this.getText());
        } catch (FileNotFoundException e){
            System.out.println("Ошибка!");
        }
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "Container{" +
                "text='" + text + '\'' +
                '}';
    }
}