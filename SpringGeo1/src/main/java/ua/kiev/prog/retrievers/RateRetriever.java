package ua.kiev.prog.retrievers;

import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ua.kiev.prog.json.Rate;

import java.time.LocalTime;

@Component
public class RateRetriever {

    private CacheManager cacheManager;
    private static final String URL = "https://api.apilayer.com/exchangerates_data/latest";
    private static final String key = "GEKRm5cC3iAQUHGnzkqcku1epZJtcwB7";
    public RateRetriever(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }
    @Cacheable(value = "rates", key = "'rate'") // Redis
    public Rate getRate() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("apikey", key);
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Rate> response = restTemplate.exchange(
                URL,
                HttpMethod.GET,
                entity,
                Rate.class
        );
        return response.getBody();
    }

   @Scheduled(fixedRate = 60000)
   public void updateRate() {
       cacheManager.getCache("rates").evict("rate");
       getRate();
   }

}
