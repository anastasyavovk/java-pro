$(document).ready(function() {
    $.getJSON('/rate', function(data) {
        $('#rate').text(data.rates.UAH);
        $('#date').text(data.date);
        $.getJSON('https://api.ipify.org?format=json', function(data) {
            $('#ip').text(data.ip);
        });
    });
});